const MAX_POINTS: u32 = 100_000;

fn main() {
    println!("MAX_POINTS const value is: {}", MAX_POINTS);
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
}
