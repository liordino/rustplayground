use std::io;

fn main() {
    loop {
        println!("Inform the temperature to be converted:");

        let mut temperature = String::new();
        io::stdin().read_line(&mut temperature)
            .expect("Failed to read temperature!");
            
        let temperature: f32 = match temperature.trim().parse() {
            Ok(num) => num,
            Err(_) => continue
        };

        println!("Now select one of the following options:");
        println!("  1. Convert from Fahrenheit to Celsius;");
        println!("  2. Convert from Celsius to Fahrenheit;");

        let mut option = String::new();
        io::stdin().read_line(&mut option)
            .expect("Failed to read option!");
            
        let option: i32 = match option.trim().parse() {
            Ok(num) => num,
            Err(_) => continue
        };

        match option {
            1 => 
                println!("{}º Fahrenheit => {}º Celsius", 
                    temperature, 
                    fahrenheit_to_celsius(temperature)),
            2 => 
                println!("{}º Celsius => {}º Fahrenheit", 
                    temperature, 
                    celsius_to_fahrenheit(temperature)),
            _ => continue
        }
    }
}

fn fahrenheit_to_celsius(temperature: f32) -> f32 {
    (temperature - 32.0) / 1.8
}

fn celsius_to_fahrenheit(temperature: f32) -> f32 {
    (temperature * 1.8) + 32.0
}