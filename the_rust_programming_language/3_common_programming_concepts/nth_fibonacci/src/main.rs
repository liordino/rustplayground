use std::io;

fn main() {
    loop {
        println!("Inform the position on the Fibonacci sequence and I'll say which number is at it:");
        
        let mut n = String::new();
        io::stdin().read_line(&mut n)
            .expect("Failed to read sequence position!");
                
        let n: f64 = match n.trim().parse() {
            Ok(num) => num,
            Err(_) => continue
        };
        
        // use Binet's formula to compute the nth number of the Fibonacci sequence
        let phi: f64 = (1f64 + 5f64.sqrt()) / 2f64;
        let nth_fibonacci = ((phi.powf(n) - (-1f64/phi).powf(n))/5f64.sqrt()) as i32;

        println!("The {}th number of the Fibonacci sequence is: {}", n, nth_fibonacci);
    }
}
