fn main() {
    let mut counter = 0;

    let result = loop {
        counter += 1;
        println!("COUNTING UP! {}", counter);

        if counter == 10 {
            break counter * 2;
        }
    };

    assert_eq!(result, 20);

    while counter != 0 {
        println!("COUNTING DOWN! {}", counter);
        counter -= 1;
    }

    println!("LIFTOFF!!!");

    let a = [10, 20, 30, 40, 50];
    
    for element in a.iter() {
        println!("The value is: {}", element);
    }

    for number in (1..10).rev() {
        println!("COUNTING DOWN WITH FOR! {}", number);
    }

    println!("LIFTOFF WITH FOR!!!");
}
