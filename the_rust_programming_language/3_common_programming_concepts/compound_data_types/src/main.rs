fn main() {
    let tuple: (i32, f64, u8) = (500, 6.4, 1);
    println!("We can print a tuple using its elements indexes... ({}, {}, {})", tuple.0, tuple.1, tuple.2);

    let (x, y, z) = tuple;
    println!("... or pattern matching and tuple destructuring ({}, {}, {})", x, y, z);

    let array = [1, 2, 3];
    println!("We also have arrays [{}, {}, {}]", array[0], array[1], array[2]);

    let array: [i32; 5] = [4, 5, 6, 7, 8];
    println!("And can declare it's type and size explicitly [{}, {}, {}, {}, {}]", array[0], array[1], array[2], array[3], array[4]);
    
    let array: [i32; 2] = [9, 10];
    println!("Trying to access an invalid index makes rust panic:");
    let invalid_index = 2;
    println!("{}", array[invalid_index]);
}
