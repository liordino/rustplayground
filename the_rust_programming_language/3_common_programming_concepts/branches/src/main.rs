fn main() {
    let number = if true { 
        7
    } else {
        6
    };

    println!("Number is {}", number);
    
    if number < 5 {
        println!("Number is smaller than 5");
    } else {
        println!("Number is bigger than 5");
    }

    if number % 4 == 0 {
        println!("Number is divisible by 4");
    } else if number % 3 == 0 {
        println!("Number is divisible by 3");
    } else if number % 2 == 0 {
        println!("Number is divisible by 2");
    } else {
        println!("Number is not divisible by 4, 3 or 2");
    }
}
