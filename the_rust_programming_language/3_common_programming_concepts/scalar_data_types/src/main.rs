fn main() {
    let _a = 1; // 32 bit integer
    let _a: i64 = 1; // 64 bit integer
    println!("The integer value is: {}", _a);
    let _a = 1.5; // 64 bit floating point
    let _a: f32 = 1.5; // 32 bit floating point
    println!("The floating point value is: {}", _a);
    let _a = true;
    println!("The boolean is: {}", _a);
    let _a = 'd';
    println!("The char is: {}", _a);
    let _a = '😻';
    println!("We can an heart eyed cat! {}", _a);
}
