fn main() {
    let _x: i32 = 5;
    let _y: i32 = 3;
    print_integers(_x, _y);
    println!("The sum of x and y is: {}", sum_integers(_x, _y));
}

fn print_integers(x: i32, y: i32) {
    println!("The value of the (x, y) pair is: ({}, {})", x, y);
}

fn sum_integers(x: i32, y: i32) -> i32 {
    x + y
}