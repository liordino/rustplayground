fn main() {
    // Same type shadowing
    let x = 5;
    let x = x + 1;
    let x = x * 2;
    println!("The value of x is: {}", x);

    // Changing type when shadowing
    let spaces = "   ";
    let spaces = spaces.len();
    println!("{} spaces", spaces);
}
